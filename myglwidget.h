#ifndef MYGLWIDGET_H
#define MYGLWIDGET_H

#include <QWidget>
#include <QOpenGLWidget>
#include <QKeyEvent>

class MyGLWidget : public QOpenGLWidget
{
  Q_OBJECT

public:
  MyGLWidget();
  MyGLWidget(QWidget*& w);


  // QGLWidget interface
protected:
  void initializeGL();
  void resizeGL(int w, int h);
  void paintGL();

  // QGLWidget interface
public slots:
//  void updateGL();
//  void updateOverlayGL();
  void receiveRotationZ(int value);

private:
  unsigned int counter = 0;
  unsigned int rotation = 0;
  double posX = 0.0;
  double posY = 0.0;
  double posZ = 0.0;

  // QWidget interface
protected:
  void keyPressEvent(QKeyEvent *event);

  // QWidget interface
protected:
  void wheelEvent(QWheelEvent *event);

signals:
  void signalZoomZ(double);
};

#endif // MYGLWIDGET_H
