#include "myglwidget.h"
#include <iostream>

MyGLWidget::MyGLWidget()
{
  setFocusPolicy(Qt::StrongFocus);
}

MyGLWidget::MyGLWidget(QWidget*& w) : QOpenGLWidget(w)
{
  setFocusPolicy(Qt::StrongFocus);

}

void MyGLWidget::initializeGL()
{
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_CULL_FACE);//Disabled by default
  glDepthFunc(GL_LEQUAL);
  glShadeModel(GL_SMOOTH);//glShadeModel(GL_FLAT); Flat only picks the last color of vertices
  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

  glClearDepth(1.0f);
  glClearColor(.0f, .0f, .0f, .0f);//Set bgColor black
}

void MyGLWidget::resizeGL(int width, int height)
{
  // Compute aspect ratio
  height = (height == 0) ? 1 : height;
  //GLfloat aspect = (GLfloat)width / (GLfloat)height;

  // Set viewport to cover the whole window
  glViewport(0, 0, width, height);

  // Set projection matrix to a perspective projection
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  //gluPerspective(45.0f, aspect, 0.1, 100.0);//Perspective view
  //glOrtho(-2.0,2.0,-2.0,2.0,0.1,10.0); //orthogonal view
  glFrustum(-0.05,0.05,-0.05,0.05,0.1,100.0);
}

void MyGLWidget::paintGL()
{
  // Clear buffer to set color and alpha
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // Apply model view transformations
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glTranslatef(0.0f+this->posX, 0.0f+this->posY, -7.0f/* + this->posZ*/);
  glScalef( 1 + this->posZ, 1 + this->posZ, 1 + this->posZ);
  // Set color for drawing
  glColor4f(1.0f, 0.0f, 0.0f, 1.0f);

  // Rotate shape
  glRotatef(45 + this->rotation + this->counter,0,0,1);//rotation of this->counter° around z axis
  glRotatef(180,0,1,0);// rotation of 180° around y axis to see the back of the object
  // Draw shape
  float x = .0f, y = .0f, z = .0f;

  //front
  glBegin(GL_QUADS);
    glColor3f(1.0f, 0.0f, 0.0f);
    glVertex3f( 1.0f + x, -1.0f + y,  1.0f + z);//Slight modification of he location of the triangle
    glColor3f(0.0f, 1.0f, 0.0f);
    glVertex3f( 1.0f + x, 1.0f + y,  1.0f + z);
    glColor3f(0.0f, 0.0f, 1.0f);
    glVertex3f(-1.0f + x,  1.0f + y, 1.0f + z);
    glColor3f(1.0f, 1.0f, 1.0f);
    glVertex3f( -1.0f + x, -1.0f + y, 1.0f + z);
  glEnd();
  //top
  glBegin(GL_QUADS);
      glColor3f(1.0f, 0.0f, 0.0f);
      glVertex3f( 1.0f + x, 1.0f + y, -1.0f + z);//Slight modification of he location of the triangle
      glColor3f(0.0f, 1.0f, 0.0f);
      glVertex3f( -1.0f + x,  1.0f + y, -1.0f + z);
      glColor3f(0.0f, 0.0f, 1.0f);
      glVertex3f(-1.0f + x,  1.0f + y,  1.0f + z);
      glColor3f(1.0f, 1.0f, 1.0f);
      glVertex3f(1.0f + x, 1.0f + y,  1.0f + z);
  glEnd();
  //back
  glBegin(GL_QUADS);
      glColor3f(1.0f, 0.0f, 0.0f);
      glVertex3f( 1.0f + x, 1.0f + y,  -1.0f + z);//Slight modification of he location of the triangle
      glColor3f(0.0f, 0.0f, 1.0f);
      glVertex3f( 1.0f + x, -1.0f + y, -1.0f + z);
      glColor3f(1.0f, 1.0f, 1.0f);
      glVertex3f(-1.0f + x, -1.0f + y, -1.0f + z);
      glColor3f(0.0f, 1.0f, 0.0f);
      glVertex3f( -1.0f + x, 1.0f + y, -1.0f + z);
  glEnd();
  //bottom
  glBegin(GL_QUADS);
      glColor3f(1.0f, 0.0f, 0.0f);
      glVertex3f( 1.0f + x, -1.0f + y,  -1.0f + z);//Slight modification of he location of the triangle
      glColor3f(0.0f, 1.0f, 0.0f);
      glVertex3f( 1.0f + x, -1.0f + y, 1.0f + z);
      glColor3f(0.0f, 0.0f, 1.0f);
      glVertex3f(-1.0f + x, -1.0f + y,  1.0f + z);
      glColor3f(1.0f, 1.0f, 1.0f);
      glVertex3f(-1.0f + x, -1.0f + y, -1.0f + z);
  glEnd();
  //left
  glBegin(GL_QUADS);
      glColor3f(1.0f, 0.0f, 0.0f);
      glVertex3f( -1.0f + x, 1.0f + y, -1.0f + z);//Slight modification of he location of the triangle
      glColor3f(0.0f, 1.0f, 0.0f);
      glVertex3f( -1.0f + x, -1.0f + y, -1.0f + z);
      glColor3f(0.0f, 0.0f, 1.0f);
      glVertex3f(-1.0f + x, -1.0f + y, 1.0f + z);
      glColor3f(1.0f, 1.0f, 1.0f);
      glVertex3f(-1.0f + x, 1.0f + y,  1.0f + z);
  glEnd();
  //right
  glBegin(GL_QUADS);
      glColor3f(1.0f, 0.0f, 0.0f);
      glVertex3f( 1.0f + x, 1.0f + y, 1.0f + z);//Slight modification of he location of the triangle
      glColor3f(0.0f, 1.0f, 0.0f);
      glVertex3f( 1.0f + x, -1.0f + y, 1.0f + z);
      glColor3f(0.0f, 0.0f, 1.0f);
      glVertex3f(1.0f + x,  -1.0f + y, -1.0f + z);
      glColor3f(1.0f, 1.0f, 1.0f);
      glVertex3f(1.0f + x, 1.0f + y, -1.0f + z);
  glEnd();

  // Execute all issued GL commands
  //glFlush(); // replace with glutSwapBuffers() for double buffered mode

  // Increment this->counter
  this->counter++;
  this->update();
}

void MyGLWidget::receiveRotationZ(int value)
{
  this->rotation = value;
}

void MyGLWidget::keyPressEvent(QKeyEvent *event)
{

  switch(event->key())
  {
    case Qt::Key_Left:
    case Qt::Key_A:
      std::cout << "event left" << std::endl;
      this->posX-=.1;
      break;
    case Qt::Key_Up:
    case Qt::Key_W:
      std::cout << "event up" << std::endl;
      this->posY+=.1;
      break;
    case Qt::Key_Right:
    case Qt::Key_D:
      std::cout << "event right" << std::endl;
      this->posX+=.1;
      break;
    case Qt::Key_Down:
    case Qt::Key_S:
      std::cout << "event down" << std::endl;
      this->posY-=.1;
      break;
    default:
      QOpenGLWidget::keyPressEvent(event);
      break;

  }
}

void MyGLWidget::wheelEvent(QWheelEvent *event)
{
  if(event->delta() < 0 && this->posZ > -0.99){
    this->posZ-=0.01;
  }
  if(event->delta() > 0){
    this->posZ+=0.01;
  }
  std::cout << "mousewheel: " << event->delta() << std::endl
            << "pos z: " << this->posZ << std::endl;
  emit signalZoomZ(this->posZ+1);
}

/*void MyGLWidget::updateGL()
{


  // Schedule a draw event
  //glutPostRedisplay();

  // Needs to be registered again
  //QGLWidget::glDraw();
}

void MyGLWidget::updateOverlayGL(){

}*/
